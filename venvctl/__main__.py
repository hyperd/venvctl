"""DO NOT REMOVE THIS."""

from __future__ import (absolute_import, division, print_function)
from venvctl.cli.main import run

if __name__ == '__main__':
    run()
